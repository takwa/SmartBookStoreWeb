<?php

namespace SmartBook\LecteurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SmartBookLecteurBundle:Default:index.html.twig', array('name' => $name));
    }
}
